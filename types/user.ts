export interface Address {
    streetAddress: string,
    city: string,
    state: string,
    zip: string
}

export interface User {
    id: number,
    fullname: string,
    company: string,
    email: string,
    uname: string,
    address: Address,
    searchTags?: string
}
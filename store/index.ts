import { Store } from 'vuex'
import { getModule } from 'vuex-module-decorators'
import User from "~/store/User"

let userStore: User

const initializer = (store: Store<any>) => {
  userStore = getModule(User, store);
}

export const plugins = [initializer];

export {
  userStore
}

import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import axios from '~/plugins/axios'

import { User as UserSchema } from '@/types/user'
import { Filter as FilterSchema } from "@/types/filter";

@Module({
    name: 'User',
    stateFactory: true,
    namespaced: true,
})
export default class User extends VuexModule {
    private users: Array<UserSchema> = [];

    private filters: Array<FilterSchema> = [
        {
            title: "Full name",
            value: "fullname",
        },
        {
            title: "Username",
            value: "uname",
        },
        {
            title: "E-mail",
            value: "email",
        },
        {
            title: "Company",
            value: "company",
        },
        {
            title: "State",
            value: "address.state",
        },
    ]

    private modalFields: Array<FilterSchema> = [
        {
            title: 'name',
            value: 'fullname'
        },
        {
            title: 'street address',
            value: 'address.streetAddress'
        },
        {
            title: 'city',
            value: 'address.city'
        },
        {
            title: 'state',
            value: 'address.state'
        },
        {
            title: 'zip',
            value: 'address.zip'
        },
    ]

    get USERS(): Array<UserSchema> {
        return this.users;
    }

    get FILTERS(): Array<FilterSchema> {
        return this.filters;
    }

    get MODAL_FIELDS(): Array<FilterSchema> {
        return this.modalFields;
    }

    @Mutation
    setUsers(users: Array<UserSchema>): void {
        this.users = users;
    }

    @Action
    async GET_USERS(): Promise<void> {
        const data: any = (await axios.get('http://www.filltext.com/?rows=1000&id={index}&fullname={firstName}~{lastName}&company={business}&email={email}&uname={username}&address={addressObject}')
            .catch(err => {
                console.log(err);
            }))
        this.setUsers(data.data)
    }
}
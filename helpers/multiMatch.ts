import { Filter as FilterSchema } from "@/types/filter";
import { propParser } from "./propParser";

export function multiMatch(item: any, fields: Array<FilterSchema>, filterText: string): boolean {
    let result: boolean = false;
    fields.forEach(field => {
        if (propParser(item, field.value).indexOf(filterText) !== -1) {
            result = true;
        }
    });
    return result;
}
import { propParser } from "./propParser"

export function sortByParams(items: Array<any>, param: string, reverse: boolean): Array<any> {
    if (param === "") return items;
    let result: Array<any> = items.slice().sort((a, b): number => {
        let compareResult = propParser(a, param) > propParser(b, param);

        if (!reverse) {
            return compareResult ? 1 : -1;
        } else {
            return compareResult ? -1 : 1;
        }
    });
    return result;
}
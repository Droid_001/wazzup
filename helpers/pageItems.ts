//return <resultsPerList> items for <currentPage> from <items>
export function pageItems(items: Array<any>, resultsPerList: number, currentPage: number): Array<any> {
    return items.slice(
        (currentPage - 1) * resultsPerList,
        currentPage * resultsPerList
    );
}
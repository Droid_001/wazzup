//"Deep" parsing for object
export function propParser(obj: any, props: string): string {
    let arr: string[] = props.split(".", 1000);
    let result: any = obj;

    arr.forEach((key) => {
        result = result[key];
    });

    return result;
}